import jwt from 'jsonwebtoken'

const TOKEN_KEY = '&%$S1stem45==_+'

export const createToken = (user) => {
    let payload = { 
        id: user.id,
        name: user.name,
        type: user.type
    }

    return jwt.sign(payload, TOKEN_KEY, { subject: user.id, expiresIn: 600})
}

export const readToken = (token) => {
    if (token != null && token !== '') {
        try {
            return jwt.verify(token.substring('Bearer '.length), TOKEN_KEY)
        } catch(error) {
        }
    } 

    return null
}